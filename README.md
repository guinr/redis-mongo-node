# Pré requisites

1. Start MongoDB Container
2. Start Redis Container
3. Run node src/app

Now you can CRUD by HTTP requests.

Notice that when you GET, to bring data from the Person table, in the first request
data will be brought through mongoDB and stored in Redis for 10 seconds.
Before cache expires, if you GET data will be returned by Redis instead of mongoDB.